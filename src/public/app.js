
console.log("app.js");

fetch('./output/wonTossWonMatch.json')
.then(data => data.json())
.then(visualizeWonTossWonMatch)
.catch((error) => {
	console.error(error);
});

function visualizeWonTossWonMatch(seriesData) {
  
    Highcharts.chart('won-toss-and-match', {
        chart: {
          type: 'column',
        },
        title: {
          text: 'Number of times a team has won toss and the match',
        },
        xAxis: {
          type: 'category',
          labels: {
            rotation: -45,
            style: {
              fontSize: '12px',
              fontFamily: 'Verdana, sans-serif',
            }
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Number of Wins',
          }
        },
        legend: {
          enabled: false,
        },
        tooltip: {
          pointFormat: 'Number of won toss and match: <b>{point.y}</b>',
        },
        series: [{
          name: 'Number of Wins',
          data: Object.entries(seriesData),
          dataLabels: {
            enabled: true,
            rotation: 0,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            y: 25, // 10 pixels down from the top
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif',
            }
          }
        }]
      });
  }

fetch('./output/matchesPlayedPerYear.json')
.then(data => data.json())
.then(visualizeMatchesPlayedPerYear);

function visualizeMatchesPlayedPerYear(seriesData) {
  
    Highcharts.chart('matches-played-per-year', {
        chart: {
          type: 'column',
        },
        title: {
          text: 'Matches Played Per year',
        },
        xAxis: {
          type: 'category',
          labels: {
            rotation: -45,
            style: {
              fontSize: '12px',
              fontFamily: 'Verdana, sans-serif',
            }
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Number of Matches',
          }
        },
        legend: {
          enabled: false,
        },
        tooltip: {
          pointFormat: 'Matches Player Per Year: <b>{point.y}</b>'
        },
        series: [{
          name: 'Number of Matches',
          data: Object.entries(seriesData),
          dataLabels: {
            enabled: true,
            rotation: 0,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            y: 25, // 10 pixels down from the top
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif',
            }
          }
        }]
      });
  }
  

  fetch('./output/extraRunsPerTeam.json')
.then(data => data.json())
.then(visualizeExtraRunsPerTeam)

function visualizeExtraRunsPerTeam(seriesData) {
  
    Highcharts.chart('extra-runs-per-team', {
        chart: {
          type: 'column',
        },
        title: {
          text: 'Extra runs per team',
        },
        xAxis: {
          type: 'category',
          labels: {
            rotation: -45,
            style: {
              fontSize: '12px',
              fontFamily: 'Verdana, sans-serif',
            }
          }
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Extra Runs',
          }
        },
        legend: {
          enabled: false,
        },
        tooltip: {
          pointFormat: 'Extra runs: <b>{point.y}</b>',
        },
        series: [{
          name: 'Extra runs',
          data: Object.entries(seriesData),
          dataLabels: {
            enabled: true,
            rotation: 0,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y}', // one decimal
            y: 25, // 10 pixels down from the top
            style: {
              fontSize: '13px',
              fontFamily: 'Verdana, sans-serif'
            }
          }
        }]
      });
  }
  
  fetch('./output/topEconomicBowlers.json')
  .then(data => data.json())
  .then(visualizeTopEconomicBowler)
  
  function visualizeTopEconomicBowler(seriesData) {
    
      Highcharts.chart('top-economic-bowlers', {
          chart: {
            type: 'column',
          },
          title: {
            text: 'Top Economic Bowlers',
          },
          xAxis: {
            type: 'category',
            labels: {
              rotation: -45,
              style: {
                fontSize: '12px',
                fontFamily: 'Verdana, sans-serif',
              }
            }
          },
          yAxis: {
            min: 0,
            title: {
              text: 'Top Economic Bowlers',
            }
          },
          legend: {
            enabled: false,
          },
          tooltip: {
            pointFormat: 'Top Economic Bowlers: <b>{point.y:.1f}</b>'
          },
          series: [{
            name: 'Top Economic Bowlers',
            data: Object.entries(seriesData),
            dataLabels: {
              enabled: true,
              rotation: 0,
              color: '#FFFFFF',
              align: 'right',
              format: '{point.y:.1f}', // one decimal
              y: 25, // 10 pixels down from the top
              style: {
                fontSize: '13px',
                fontFamily: 'Verdana, sans-serif',
              }
            }
          }]
        });
    }
    
fetch('./output/matchesWonPerYear.json')
.then(data => data.json())
.then(visualizeMatchesWonPerYear);

function visualizeMatchesWonPerYear(seriesData) {

    let years = Object.keys(Object.values(seriesData)[0]);
    let dataArray = Object.entries(seriesData);
    let finalData = dataArray.reduce((accumulator, value) => {
      let obj = {};
      obj['name'] = value[0];
      obj['data'] = Object.values(value[1]);
      accumulator.push(obj);
      return accumulator;
    }, []);
    console.log(finalData);

    Highcharts.chart('matches-won-per-year', {
        chart: {
          type: 'column',
        },
        title: {
          text: 'Matches Won Per Year',
        },
        subtitle: {
          text: 'Source: <a href=\'https://www.kaggle.com/manasgarg/ipl\'>Kaggle</a>',
        },
        xAxis: {
          categories: years,
          crosshair: true,
        },
        yAxis: {
          min: 0,
          title: {
            text: 'Matches Won',
          }
        },
        tooltip: {
          headerFormat: '<span style=\'font-size:10px\'>{point.key}</span><table>',
          pointFormat: '<tr><td style=\'color:{series.color};padding:0\'>{series.name}: </td>' +
            '<td style=\'padding:0\'><b>{point.y}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true,
        },
        plotOptions: {
          column: {
            pointPadding: 0.2,
            borderWidth: 0,
          }
        },
        series: finalData,
      });
}

fetch('./output/strikeRateBatsman.json')
.then(data => data.json())
.then(visualizeStrikeRate)

function visualizeStrikeRate(seriesData) {
  // Default - DA Warner - OR Take User's input
  let data = seriesData['DA Warner'];
  let dataObject = {};
  dataObject['name'] = 'DA Warner';
  dataObject['data'] = Object.values(data);

  Highcharts.chart('strike-rate-batsman', {

    title: {
      text: 'Strike rate of Batsman',
    },
    subtitle: {
      text: 'Source: <a href=\'https://www.kaggle.com/manasgarg/ipl\'>Kaggle</a>',
    },
    yAxis: {
      title: {
        text: 'Strike Rate',
      },
    },
    xAxis: {
      accessibility: {
        rangeDescription: 'Range: 2010 to 2017',
      },
    },
    legend: {
      layout: 'vertical',
      align: 'right',
      verticalAlign: 'middle',
    },
    plotOptions: {
      series: {
        label: {
          connectorAllowed: false,
        },
        pointStart: 2008,
      },
    },
    series: [dataObject],
    responsive: {
      rules: [{
        condition: {
          maxWidth: 500,
        },
        chartOptions: {
          legend: {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
          },
        },
      }],
    },
  });
}

fetch('./output/playerDismissalCount.json')
.then(data => data.json())
.then(visualizePlayerDismissalCount)

function visualizePlayerDismissalCount(seriesData) {

  let finalData = Object.entries(seriesData);
  let labelData = {};
  finalData.forEach((value, index, array) => {
    labelData[array[index][0]] = array[index][1][0];
    array[index][1] = array[index][1][1];
  })

  Highcharts.chart('player-dismissal-count', {
    chart: {
      type: 'column',
      scrollablePlotArea: {
        minWidth: 10000,
        scrollPositionX: 1,
      }
    },
    title: {
      text: 'Player Dismissal Count',
    },
    subtitle: {
      text: 'Source: <a href=\'https://www.kaggle.com/manasgarg/ipl\'>Kaggle</a>',
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -90,
        style: {
          fontSize: '10px',
          fontFamily: 'Verdana, sans-serif',
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Number of Dismissals',
      }
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      formatter: function () {
        return 'Dismissed by <b>' + labelData[this.point.name] + '<b> - <b>'+ this.point.y + '</b> times<br/>';
      },
      pointFormat: 'Number of dismissals: <b>{point.y:.1f} {point.name} {point.x}millions</b>',
    },
    series: [{
      name: 'Population',
      data: finalData,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        }
      }
    }]
  });
}

fetch('./output/playerOfMatchCount.json')
.then(data => data.json())
.then(visualizePlayerOfMatchCount);

function visualizePlayerOfMatchCount(seriesData) {

  let finalData = Object.entries(seriesData);
  let labelData = {};
  finalData.forEach((value, index, array) => {
    labelData[array[index][0]] = array[index][1][0];
    array[index][1] = array[index][1][1]; 
  })
  console.log(finalData);

  Highcharts.chart('player-of-match-count', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Player of Match Count',
    },
    subtitle: {
      text: 'Source: <a href=\'https://www.kaggle.com/manasgarg/ipl\'>Kaggle</a>',
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: 0,
        style: {
          fontSize: '10px',
          fontFamily: 'Verdana, sans-serif',
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Number of Player of Match',
      }
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      formatter () {
        return '<b>' + labelData[this.point.name] + '<b> won <b>'+ this.point.y + '</b> player of match awards<br/>';
      },
    },
    series: [{
      name: 'Population',
      data: finalData,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        }
      }
    }]
  });
}

fetch('./output/bowlerBestEconomy.json')
.then(data => data.json())
.then(visualizeBowlerBestEconomy);

function visualizeBowlerBestEconomy(seriesData) {
  Highcharts.chart('bowler-best-economy', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'Bowlers with best economy in super overs',
    },
    subtitle: {
      text: 'Source: <a href=\'https://www.kaggle.com/manasgarg/ipl\'>Kaggle</a>',
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Economy',
      }
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: 'Best Economy: <b>{point.y:.1f}</b>',
    },
    series: [{
      name: 'Economy',
      data: Object.entries(seriesData),
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        format: '{point.y:.1f}', // one decimal
        y: 10, // 10 pixels down from the top
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        }
      }
    }]
  });
}
