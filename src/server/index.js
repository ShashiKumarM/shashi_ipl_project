const http = require('http');
const fs = require('fs');
const path = require('path');

http.createServer((request, response) => {
	
	if(request.method === 'GET') {

		let pathname = `../public${request.url}`;

		if(request.url === '/') pathname += 'index.html';
		
		const extName = path.extname(pathname);

		const mimeTypes = {
      		'.html' : 'text/html',
			'.css' : 'text/css',
			'.json' : 'application/json',
			'.js' : 'text/javascript',
		};

		const contentType = mimeTypes[extName];
		
		console.log('pathname', pathname, contentType);

		fs.readFile(path.join(__dirname, pathname), 'utf-8', (err, data) => {
		
			if(err) {
				response.writeHead(404, { 'content-type' : 'text/plain' });
				response.end('ERROR : File not found');
			}
			response.writeHead(200, { 'content-type': contentType });
			response.end(data);
		});
	}

}).listen(3000, () => console.log('Server listening at port 3000'));
