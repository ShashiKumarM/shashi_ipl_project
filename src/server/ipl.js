/**
 *  Module to process the data and create public json data
 */

const fs = require('fs');
const path = require('path');
const csv = require('csvtojson');

const matchesPlayedPerYear = require(path.join(__dirname, './ipl_modules/matchesPlayedPerYear')); 
const matchesWonPerYear = require(path.join(__dirname, './ipl_modules/matchesWonPerYear'));
const extraRunsPerTeam = require(path.join(__dirname, './ipl_modules/extraRunsPerTeam'));
const topEconomicBowlers = require(path.join(__dirname, './ipl_modules/topEconomicBowlers'));
const strikeRateBatsman = require(path.join(__dirname, './ipl_modules/strikeRateBatsman'));
const wonTossAndMatch = require(path.join(__dirname, './ipl_modules/wonTossAndMatch'));
const playerOfMatchCount = require(path.join(__dirname, './ipl_modules/playerOfMatchCount'));
const playerDismissalCount = require(path.join(__dirname, './ipl_modules/playerDismissalCount'));
const bowlerBestEconomy = require(path.join(__dirname, './ipl_modules/bowlerBestEconomy'));

const MATCHES_FILE_PATH = path.join(__dirname, '../data/matches.csv');
const DELIVERIES_FILE_PATH = path.join(__dirname, '../data/deliveries.csv')

console.log('Computing....');

function saveJsonData(fileName, fileData) {
  return new Promise((resolve, reject) => {
    fs.writeFile(path.join(__dirname, '../public/output/', fileName, '.json'), fileData, 'utf-8', (error) => {
      if (error) {
        reject(error);
      } else {
        resolve(`${fileName} created\n`);
      }
    });
  });
}


Promise.all([csv().fromFile(MATCHES_FILE_PATH), csv().fromFile(DELIVERIES_FILE_PATH)])
  .then(([matches, deliveries]) => {
    const mainDataObject = {};

    mainDataObject.matchesPlayedPerYear = matchesPlayedPerYear(matches);
    mainDataObject.matchesWonPerYear = matchesWonPerYear(matches);
    mainDataObject.playerOfMatchCount = playerOfMatchCount(matches);
    mainDataObject.wonTossWonMatch = wonTossAndMatch(matches);

    mainDataObject.topEconomicBowlers = topEconomicBowlers(matches, deliveries, '2015');
    mainDataObject.bowlerBestEconomy = bowlerBestEconomy(deliveries);
    mainDataObject.extraRunsPerTeam = extraRunsPerTeam(matches, deliveries, '2016');
    mainDataObject.playerDismissalCount = playerDismissalCount(deliveries);
    mainDataObject.strikeRateBatsman = strikeRateBatsman(matches, deliveries);

    console.log('Completed, Saving data....');

    return mainDataObject;
  })
  .then((mainDataObject) => {
    const saveJsonPromises = [];

    for (const key in mainDataObject) {
    
      const jsonFileData = JSON.stringify(mainDataObject[key]);
      saveJsonPromises.push(saveJsonData(key, jsonFileData));
    }

    return Promise.all(saveJsonPromises);
  })
  .then((saveDataResult) => {

    console.log(saveDataResult);
  })
  .catch((error) => {

    console.error(error);
  })
  .finally(() => {

    console.log('Data Saved');
  });
