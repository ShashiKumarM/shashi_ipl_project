/**
 * Module to find the highest number of times a player has been dismissed by another player
 * @param {Object} deliveries Deliveries data object
 * 
 */

module.exports = function playerDismissalCount(deliveries) {

	let batsmanBowler = deliveries.reduce((accumulator, delivery) => {
		
		if(delivery.player_dismissed.length && 
			delivery.dismissal_kind !== 'run out' && 
			delivery.dismissal_kind !== 'retired hurt' && 
			delivery.dismissal_kind !== 'hit wicket' && 
			delivery.dismissal_kind !== 'obstructing the field'
			) {

			if(accumulator.hasOwnProperty(delivery.player_dismissed)) {
				
				if(accumulator[delivery.player_dismissed].hasOwnProperty(delivery.bowler)) {
					accumulator[delivery.player_dismissed][delivery.bowler] += 1;
				} else {
					accumulator[delivery.player_dismissed][delivery.bowler] =  1;
				}
			} else {
				
				accumulator[delivery.player_dismissed] = {};
				accumulator[delivery.player_dismissed][delivery.bowler] = 1;
			}	
		}

		return accumulator;
	}, {})

	for(let key in batsmanBowler) {

		batsmanBowler[key] = Object.entries(batsmanBowler[key]).sort((bowlerOne, bowlerTwo) => {
			
			return bowlerTwo[1] - bowlerOne[1];
				
		})[0];
	}

	return batsmanBowler;
}
