/**
 * Module computes extra runs conceded by teams in the IPL season 2016
 * @param {Object} matches Matches data Object
 * @param {Object} deliveries Deliveries data object
 * @param {String} season A string value of season
 */

module.exports = function extraRunsPerTeam(matches, deliveries, season) {

	let matchIds = matches.reduce((accumulator, match) => {

		if(match.season === season) {
			accumulator.push(match.id);
		}

		return accumulator;
	}, []);
		
	let extraRunsPerTeam = deliveries.reduce((accumulator, delivery) => {

		if(matchIds.includes(delivery.match_id)){

			if(accumulator.hasOwnProperty(delivery.bowling_team)) {
				accumulator[delivery.bowling_team] += parseInt(delivery.extra_runs);
			} else {
				accumulator[delivery.bowling_team] = parseInt(delivery.extra_runs);
			}
		} 

		return accumulator;
	}, {});

	return extraRunsPerTeam;
}
