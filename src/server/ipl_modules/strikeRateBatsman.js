/**
 * Module to find strike rate of batsmen 
 * @param {Object} matches Matches data object
 * @param {Object} deliveries Deliveries data object
 */

module.exports = function strikeRateBatsman(matches, deliveries) {
	
	let matchIdsAndSeason = matches.reduce((accumulator, match) => {

		if(accumulator.hasOwnProperty(match.season)) {

			if(!accumulator[match.season].includes(match.id)) {
				accumulator[match.season].push(match.id);
			}
		} else {

			accumulator[match.season] = [];
			accumulator[match.season].push(match.id);
		}
		return accumulator;
		
	}, {});

	let strikeRate = deliveries.reduce((accumulator, delivery) => {

		let season = Object.entries(matchIdsAndSeason).find(seasonAndIds => {

			return seasonAndIds[1].includes(delivery.match_id);
		})[0];

		if(accumulator.hasOwnProperty(delivery.batsman)) {
			
			if(accumulator[delivery.batsman].hasOwnProperty(season)) {

				accumulator[delivery.batsman][season][0] += parseInt(delivery.batsman_runs);
				accumulator[delivery.batsman][season][1] += 1;
			} else {

				accumulator[delivery.batsman][season] = [parseInt(delivery.batsman_runs), 1];
			} 

		} else {

			accumulator[delivery.batsman] = {};
			accumulator[delivery.batsman][season] = [parseInt(delivery.batsman_runs), 1];
		}	
		
		return accumulator;
	}, {});

	let seasons = Object.keys(matchIdsAndSeason);

    for(let batsman in strikeRate) {
    
        for(let season of seasons) {

			if(strikeRate[batsman].hasOwnProperty(season))
				strikeRate[batsman][season] = parseFloat(((strikeRate[batsman][season][0] / strikeRate[batsman][season][1]) * 100).toPrecision(2));
			else 
				strikeRate[batsman][season] = 0;
		}
	}

	return strikeRate;
}