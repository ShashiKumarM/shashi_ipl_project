/**
 * Module to compute the player with highest number of player of the match awards
 * @param {Object} matches Matches data object
 * 
 */

module.exports = function playerOfMatchCount(matches) {

	let playerOfMatchCount = matches.reduce((accumulator, match) => {

		if(accumulator.hasOwnProperty(match.season)) {

			if(accumulator[match.season].hasOwnProperty(match.player_of_match)) {
				accumulator[match.season][match.player_of_match]++;
			} else {
				accumulator[match.season][match.player_of_match] = 1;
			}
		} else {

			accumulator[match.season] = {};
			accumulator[match.season][match.player_of_match] = 1;
		}

		return accumulator;
	}, {});
	
	for(let season in playerOfMatchCount) {

		let data = Object.entries(playerOfMatchCount[season]);
		data = data.sort((valueOne, valueTwo) =>  {

			return valueOne[1] - valueTwo[1];
		
		});
		
		playerOfMatchCount[season] = data[data.length - 1];
	}
	return playerOfMatchCount;
}
