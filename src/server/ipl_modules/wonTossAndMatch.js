/**
 * Module computes the number of times a team won the toss and the match
 * @param {Object} matches Matches data object
 */

module.exports = function wonTossAndMatch(matches) {

    	return matches.reduce((accumulator, match) => {

        	if(match.toss_winner === match.winner) {

            		if(accumulator.hasOwnProperty(match.toss_winner)) {

                		accumulator[match.toss_winner] += 1;
            		} else {
						
                		accumulator[match.toss_winner] = 1;
            		}
			}

			return accumulator;
    	}, {});
}
