/**
 * Module to find the top economic bowlers of season 2015
 * @param {Object} matches Matches data Object
 * @param {Object} deliveries Deliveries data object
 * @param {String} season A string value of season
 */
module.exports = function topEconomicBowlers(matches, deliveries, season) {

	let matchIds = matches.reduce((accumulator, match) => {
		
		if(match.season === season)
			accumulator.push(match.id);
		return accumulator;
   	}, []);

	let bowlerEconomy = deliveries.reduce((accumulator, delivery) => {
		
		if(matchIds.includes(delivery.match_id)) {
			
			if(accumulator.hasOwnProperty(delivery.bowler)) {
				accumulator[delivery.bowler][1] += parseInt(delivery.total_runs);	
				accumulator[delivery.bowler][0]++;
			} else {
				accumulator[delivery.bowler] = [delivery.total_runs, 1];
			}
		}
		
		return accumulator;
	}, {});
	
	for(let bowler in bowlerEconomy) {
		bowlerEconomy[bowler] = parseFloat((bowlerEconomy[bowler][0] / ( bowlerEconomy[bowler][1] / 6)).toFixed(2));
	}
	
	return Object.entries(bowlerEconomy).sort((bowlerOne, bowlerTwo) => {

		return bowlerOne[1] - bowlerTwo[1];

 	}).slice(0,10).reduce((finalTen, bowlerData ) => {

		finalTen[bowlerData[0]] = bowlerData[1];
		return finalTen;
	}, {})

}
  
