/**
 * Module computes number matches won by teams in all seasons of IPL
 * @param {Object} matches Matches data object
 * 
 */

module.exports = function matchesWonPerYear(matches) {

        let seasons = [];

	let matchesWonPerYear = matches.reduce((accumulator, match) => {

                if(!seasons.includes(match.season))
                        seasons.push(match.season);

                if(accumulator.hasOwnProperty(match.winner)) {
                
			if(accumulator[match.winner].hasOwnProperty(match.season)) {
                                accumulator[match.winner][match.season]++;
                        } else {
                                accumulator[match.winner][match.season] = 1;
                        }

                } else if(match.winner.length) {

                        accumulator[match.winner] = {};
                        accumulator[match.winner][match.season] = 1;
                }
                return accumulator;
        }, {});

        // Add zero values to season not played by the team
        for(let team in matchesWonPerYear) {
                for(let season of seasons) {
                        if(!matchesWonPerYear[team].hasOwnProperty(season))
                                matchesWonPerYear[team][season] = 0;
                }
        }
        
	return matchesWonPerYear;
}

