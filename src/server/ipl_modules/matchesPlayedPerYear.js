/**
 * Calculates the number of matches player per year
 * @param {Object} matches - Match Data Object
 */

module.exports = function matchesPlayedPerYear(matches) {

	return matches.reduce((accumulator, matchData) => {
	
		if(accumulator.hasOwnProperty(matchData.season) ) {
			accumulator[matchData.season] += 1;

        } else {
			
        	accumulator[matchData.season] = 1;
		}	
		return accumulator;
	}, {});
}
