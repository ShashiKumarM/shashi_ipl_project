/**
 * Module to find the bowler with best economy in super overs
 * @param {Object} deliveries Deliveries data object
 */

module.exports = function bowlerBestEconomy(deliveries) {
	let bowlerEconomy = deliveries.reduce((accumulator, delivery) => {
		
		if(parseInt(delivery.is_super_over) !== 0) {
	
			if(accumulator.hasOwnProperty(delivery.bowler)) {
	
				accumulator[delivery.bowler][0] += parseInt(delivery.total_runs);
				accumulator[delivery.bowler][1] += 1;
	
			} else {
	
				accumulator[delivery.bowler] = [parseInt(delivery.total_runs), 1];
			}
		}

		return accumulator;
	}, {});

	// Convert the #runs, #balls to economy 
	return Object.entries(bowlerEconomy).reduce((accumulator, economyArray) => {
		accumulator[economyArray[0]] = parseFloat((economyArray[1][0] / (economyArray[1][1] / 6)).toFixed(2));

    	return accumulator;
	}, {});
}